package com.patsorn.youthtoday;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class DiscoveryOnTab extends Activity {
	ImageButton backButton;
	ImageView imageView1,imageView2,imageView3;
	RoundImage roundImage,roundImage2,roundedImage3;
	FrameLayout frame;
	private ListView listview;
	private ListViewAdapter listViewAdapter;
	
	private ArrayList<ListEntry> entries;
	private int[] drawables = {
			
			R.drawable.label_time,
			R.drawable.label_venue,
			R.drawable.label_ticketprice
		
	};
	private String[] titles = {
			"1 st Oct 2014",
			"Venue",
			"Price"
			
	};
	private int[] icon = {
			
			R.drawable.cell_arrow,
			R.drawable.cell_arrow
		
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_discovery_on_tab);
		ActionBar actionBar = getActionBar();
		//actionBar.hide();
		actionBar.setCustomView(R.layout.header_detail); //load your layout
		ImageButton backButton = (ImageButton) actionBar.getCustomView().findViewById(R.id.textview_cancel);
		backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
            	Intent itn = new Intent (getApplicationContext(),DiscoverActivity.class);
        		startActivity(itn);
        		overridePendingTransition(R.anim.fade_in_anim, R.anim.center_to_right);
        		finish();
            }
    });
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM); 
		
		
		//Round Image
		imageView1 = (ImageView) findViewById(R.id.firstpic);
		imageView2 = (ImageView) findViewById(R.id.imageView2);
		imageView3 = (ImageView) findViewById(R.id.imageView3);
		Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.image7);
		Bitmap bm2 = BitmapFactory.decodeResource(getResources(),R.drawable.image8);
		Bitmap bm3 = BitmapFactory.decodeResource(getResources(),R.drawable.image9);
		roundImage = new RoundImage(bm);
		roundImage2= new RoundImage(bm2);
		roundedImage3 = new RoundImage(bm3);
		imageView1.setImageDrawable(roundImage);
		imageView2.setImageDrawable(roundImage2);
		imageView3.setImageDrawable(roundedImage3);
		//view matching
		listview = (ListView) findViewById(R.id.listView1);
		//data
		entries = new ArrayList<ListEntry>();
		
		//add data
		for(int i = 0;i<titles.length;i++){
			ListEntry listEntry = new ListEntry();
			listEntry.setTitle(titles[i]);
			listEntry.setDrawable(getResources().getDrawable(drawables[i]));
			//listEntry.setDrawable(getResources().getDrawable(icon[i]));
			entries.add(listEntry);
			
		}
		listViewAdapter = new ListViewAdapter();
		listview.setAdapter(listViewAdapter);
	}
	private class ListViewAdapter extends BaseAdapter {
		
		private ListViewHolder holder;

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return entries.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			convertView =LayoutInflater.from(DiscoveryOnTab.this).inflate(R.layout.item_listview_discover_on_tab, null);
			holder = new ListViewHolder();
			holder.image = (ImageView)convertView.findViewById(R.id.dis_ontab_image);
			holder.title = (TextView)convertView.findViewById(R.id.tv_dis_ontab);
			//holder.icons = (ImageView)convertView.findViewById(R.id.icon_arrow);
			//set title
			if(entries.get(position).getTitle() != null) {
				holder.title.setText(entries.get(position).getTitle());
			}
			
			//set image
			if(entries.get(position).getDrawable() != null) {
				holder.image.setImageDrawable(entries.get(position).getDrawable());
				//holder.icons.setImageDrawable(entries.get(position).getDrawable());
			}
			return convertView;
		}
		
		private class ListViewHolder {
			
			public ImageView image;
			public TextView title;
			public ImageView icons;
		}
		
	}
	
	public void goDiscover(View view){
		Intent itn = new Intent (getApplicationContext(),DiscoverActivity.class);
		startActivity(itn);
		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
		finish();
	}
	
	public void goActivity(View view){
		Intent itn = new Intent (getApplicationContext(),PageActivity.class);
		startActivity(itn);
		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
		finish();
	}
	
	public void goProfile(View view){
		Intent itn = new Intent (getApplicationContext(),ProfilePage.class);
		startActivity(itn);
		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
		finish();
	}
	public void goBack (View view){
		Intent itn = new Intent (getApplicationContext(),DiscoverActivity.class);
		startActivity(itn);
		overridePendingTransition(R.anim.fade_in_anim, R.anim.center_to_right);
		finish();
	}
	
	public void goToRegister (View view){
		Intent itn = new Intent(getApplicationContext(),RegisterPage.class);
		startActivity(itn);
		finish();
		overridePendingTransition(R.anim.right_to_center, R.anim.fade_out_anim);
		
	}
	public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_discover, menu);
        return super.onCreateOptionsMenu(menu);
    }
    
 public boolean onOptionsItemSelected(MenuItem item){
	 int id = item.getItemId();
        switch (id) {
            case R.id.menu_discover:
            	Intent itn1 = new Intent (getApplicationContext(),DiscoverActivity.class);
        		startActivity(itn1);
        		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
        		finish();
                return true;
            case R.id.menu_activity:
            	Intent itn2 = new Intent (getApplicationContext(),PageActivity.class);
        		startActivity(itn2);
        		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
        		finish();
                return true;
            case R.id.menu_profile:
            	Intent it3 = new Intent (getApplicationContext(),ProfilePage.class);
    			startActivity(it3);
    			overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
    			finish();
                return true;
            default:
            	return super.onOptionsItemSelected(item);
        }
		
 }


	
}
