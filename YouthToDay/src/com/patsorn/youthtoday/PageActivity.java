package com.patsorn.youthtoday;

import java.util.ArrayList;
import java.util.List;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class PageActivity extends Activity {
	
	ImageView imageView1;
	RoundImage roundImage;

	
	private ListView listViewAct;
	private ArrayList<String> data;
	private ArrayList<String> image;
	private Context context = null;
	View header,footer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_page);
		ActionBar actionBar = getActionBar();
		//actionBar.hide();
		actionBar.setCustomView(R.layout.header_activity); //load layout
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM); 
		
		//view matching
       listViewAct = (ListView)findViewById(R.id.listView_page_activity);
	  	//footer = getLayoutInflater().inflate(R.layout.footer_bar, null);
	   // ListView listViewAct = getListView();
	    //listViewAct.addFooterView(footer);
        
        //sample data
        data = new ArrayList<String>();
        for(int i=1;i<=2;i++){
        	data.add("Title of the event");
        }
        
        //adapter
        listViewAct.setAdapter(new MyAdapter());
        
        //event
        listViewAct.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view,
					int position, long id) {
				data.get(position);
				Intent itn = new Intent (getApplicationContext(),TicketMainActivity.class);
				startActivity(itn);
				overridePendingTransition(R.anim.right_to_center, R.anim.fade_out_anim);
				
				
			}
        	
		});
        
	}
	
	private class MyAdapter extends BaseAdapter {
		private Holder holder;

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View view, ViewGroup parent) {
			
			if (view == null) {
				view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.layout_item, null);
				holder = new Holder();
				
				holder.title = (TextView) view.findViewById(R.id.item_title);
				view.setTag(holder);
			}else {
				holder = (Holder)view.getTag();
				
			}
			holder.title.setText(data.get(position));
			
			//Round Image
				imageView1 = (ImageView)view.findViewById(R.id.image);
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.image7);
				roundImage = new RoundImage(bm);
				imageView1.setImageDrawable(roundImage);

			return view;
		}
		
		private class Holder {
			public TextView title;
		}
    	
		
	}
	 public void goDiscover (View view) {
		 Intent intent = new Intent(getApplicationContext(), DiscoverActivity.class);
			startActivity(intent);
			overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
			finish();
	 }
	 
	 public void goActivity(View view){
			Intent itn = new Intent (getApplicationContext(),PageActivity.class);
			overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
			startActivity(itn);
			finish();
		}
	 
	 public void  goProfile (View view) {
		 Intent intent = new Intent(getApplicationContext(), ProfilePage.class);
			startActivity(intent);
			overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
			finish();
	 }

	 public boolean onCreateOptionsMenu(Menu menu) {
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.menu_activity, menu);
	        return super.onCreateOptionsMenu(menu);
	    }
	    
	 public boolean onOptionsItemSelected(MenuItem item){
		 int id = item.getItemId();
	        switch (id) {
	            case R.id.menu_discover:
	            	Intent itn1 = new Intent (getApplicationContext(),DiscoverActivity.class);
	        		startActivity(itn1);
	        		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
	        		finish();
	                return true;
	            case R.id.menu_activity:
	            	Intent itn2 = new Intent (getApplicationContext(),PageActivity.class);
	        		startActivity(itn2);
	        		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
	        		finish();
	                return true;
	            case R.id.menu_profile:
	            	Intent it3 = new Intent (getApplicationContext(),ProfilePage.class);
	    			startActivity(it3);
	    			overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
	    			finish();
	                return true;
	            default:
	            	return super.onOptionsItemSelected(item);
	        }
			
	 }

}
