package com.patsorn.youthtoday;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;

public class RegisterPage extends Activity {
	ImageView imViewAndroid,backButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_page);
		ActionBar actionBar = getActionBar();
		//actionBar.hide();
		actionBar.setCustomView(R.layout.header_register); //load your layout
		ImageButton backButton = (ImageButton) actionBar.getCustomView().findViewById(R.id.textview_cancel);
		backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
            	Intent itn = new Intent(getApplicationContext(),DiscoveryOnTab.class);
        		startActivity(itn);
        		finish();
        		overridePendingTransition(R.anim.fade_in_anim, R.anim.center_to_right);
            }
    });
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM); 
		imViewAndroid = (ImageView)findViewById(R.id.blurimage);
		//imViewAndroid.setAlpha(35);
	}
	
	
	public void goBack (View view){
		Intent itn = new Intent(getApplicationContext(),DiscoveryOnTab.class);
		startActivity(itn);
		finish();
		overridePendingTransition(R.anim.fade_in_anim, R.anim.center_to_right);
		
	}
	public void goDiscover(View view){
		Intent itn = new Intent (getApplicationContext(),DiscoverActivity.class);
		startActivity(itn);
		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
		finish();
	}
 
	 public void goActivity(View view){
		Intent itn = new Intent (getApplicationContext(),PageActivity.class);
		startActivity(itn);
		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
		finish();
	}
	 
	 public void goProfile(View view){
			Intent itn = new Intent (getApplicationContext(),ProfilePage.class);
			startActivity(itn);
			overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
			finish();
		}
	 public boolean onCreateOptionsMenu(Menu menu) {
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.menu_discover, menu);
	        return super.onCreateOptionsMenu(menu);
	    }
	    
	 public boolean onOptionsItemSelected(MenuItem item){
		 int id = item.getItemId();
	        switch (id) {
	            case R.id.menu_discover:
	            	Intent itn1 = new Intent (getApplicationContext(),DiscoverActivity.class);
	        		startActivity(itn1);
	        		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
	        		finish();
	                return true;
	            case R.id.menu_activity:
	            	Intent itn2 = new Intent (getApplicationContext(),PageActivity.class);
	        		startActivity(itn2);
	        		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
	        		finish();
	                return true;
	            case R.id.menu_profile:
	            	Intent it3 = new Intent (getApplicationContext(),ProfilePage.class);
	    			startActivity(it3);
	    			overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
	    			finish();
	                return true;
	            default:
	            	return super.onOptionsItemSelected(item);
	        }
			
	 }

}
