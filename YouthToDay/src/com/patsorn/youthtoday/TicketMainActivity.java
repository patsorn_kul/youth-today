 package com.patsorn.youthtoday;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.patsorn.tabswipe.adapter.TabPagerAdapter;;

public class TicketMainActivity extends FragmentActivity implements TabListener {
	private ViewPager viewPaper;
	private TabPagerAdapter mAdapter;
	private String [] tabs = {"Ticket","Info"};
	private ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ticket_main);
		
		//Initilization
		viewPaper = (ViewPager) findViewById(R.id.pager);
		actionBar = getActionBar();
		actionBar.setCustomView(R.layout.header_ticket); //load your layout
		ImageButton backButton = (ImageButton) actionBar.getCustomView().findViewById(R.id.textview_cancel);
		backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
            	Intent itn = new Intent (getApplicationContext(),PageActivity.class);
        		startActivity(itn);
        		overridePendingTransition(R.anim.fade_in_anim, R.anim.center_to_right);
        		finish();
            }
    });
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM); 
		
		//getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM); 
		//actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FAFAFA")));
		//actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#EFEFEB"))); 
		
		//setTitle("");
		//getActionBar().setIcon(R.drawable.nav_back);
		mAdapter = new TabPagerAdapter(getSupportFragmentManager());
		
		viewPaper.setAdapter(mAdapter);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		//actionBar.setDisplayHomeAsUpEnabled(true);
		//actionBar.setDisplayShowHomeEnabled(true);  // hides action bar icon
		//actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		//adding Tabs
		for(String tab_name : tabs){
			actionBar.addTab(actionBar.newTab().setText(tab_name).setTabListener(this));
			
		}
			viewPaper.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
				
				@Override
				public void onPageSelected(int position) {
					actionBar.setSelectedNavigationItem(position);
					
				}
				
				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onPageScrollStateChanged(int arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		}
	
	 
	 public boolean onCreateOptionsMenu(Menu menu) {
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.menu_activity, menu);
	        return super.onCreateOptionsMenu(menu);
	    }
	    
	 public boolean onOptionsItemSelected(MenuItem item){
		 int id = item.getItemId();
	        switch (id) {
	            case R.id.menu_discover:
	            	Intent itn1 = new Intent (getApplicationContext(),DiscoverActivity.class);
	        		startActivity(itn1);
	        		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
	        		finish();
	                return true;
	            case R.id.menu_activity:
	            	Intent itn2 = new Intent (getApplicationContext(),PageActivity.class);
	        		startActivity(itn2);
	        		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
	        		finish();
	                return true;
	            case R.id.menu_profile:
	            	Intent it3 = new Intent (getApplicationContext(),ProfilePage.class);
	    			startActivity(it3);
	    			overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
	    			finish();
	                return true;
	            default:
	            	return super.onOptionsItemSelected(item);
	        }
			
	 }

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
	
		viewPaper.setCurrentItem(tab.getPosition());
		
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

}


