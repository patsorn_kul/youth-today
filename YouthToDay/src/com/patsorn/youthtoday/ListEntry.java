package com.patsorn.youthtoday;

import android.graphics.drawable.Drawable;

public class ListEntry {
	private String title;
	private Drawable drawables;
	private Drawable icons;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Drawable getDrawable() {
		return drawables;
	}
	public void setDrawable(Drawable drawable) {
		this.drawables = drawable;
	}
	

}
