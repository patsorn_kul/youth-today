package com.patsorn.youthtoday;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class SplashSceen extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_sceen);
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		Thread splashTread = new Thread() {
    		@Override
    		public void run() {
    			try {						
    				sleep(2000);										
    			} catch (InterruptedException e) {

    			} finally {
    				if(SplashSceen.this.hasWindowFocus()){
					//((BitmapDrawable)((ImageView)findViewById(R.id.bg_image)).getDrawable()).getBitmap().recycle();
    					startActivity(new Intent(SplashSceen.this, LoginYouth.class));
    					finish();
    				}
    			}
    		}
    	};
        splashTread.start();
    }

}
