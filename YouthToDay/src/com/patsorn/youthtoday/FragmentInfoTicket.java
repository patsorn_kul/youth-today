package com.patsorn.youthtoday;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class FragmentInfoTicket extends Fragment {
	private ImageButton btnDiscover;
	private ImageButton btnActivity;
	private ImageButton btnProfile;
	
	private ListView listview;
	private ListViewAdapter listViewAdapter;
	
	ImageView imageView1,imageView2,imageView3;
	RoundImage roundImage,roundImage2,roundedImage3;
	
	private ArrayList<ListEntry> entries;
	private int[] drawables = {
			
			R.drawable.label_time,
			R.drawable.label_venue,
			R.drawable.label_ticketprice
		
	};
	private String[] titles = {
			"1 st Oct 2014",
			"Venue",
			"Price"
			
	};

	public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle saveInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_fragment_info_ticket, container,false);
		//Round Image
				imageView1 = (ImageView) rootView.findViewById(R.id.firstpic);
				imageView2 = (ImageView) rootView.findViewById(R.id.imageView2);
				imageView3 = (ImageView) rootView.findViewById(R.id.imageView3);
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.image7);
				Bitmap bm2 = BitmapFactory.decodeResource(getResources(),R.drawable.image8);
				Bitmap bm3 = BitmapFactory.decodeResource(getResources(),R.drawable.image9);
				roundImage = new RoundImage(bm);
				roundImage2= new RoundImage(bm2);
				roundedImage3 = new RoundImage(bm3);
				imageView1.setImageDrawable(roundImage);
				imageView2.setImageDrawable(roundImage2);
				imageView3.setImageDrawable(roundedImage3);
		
		listview = (ListView) rootView.findViewById(R.id.listView1);
		//data
				entries = new ArrayList<ListEntry>();
				
				//add data
				for(int i = 0;i<titles.length;i++){
					ListEntry listEntry = new ListEntry();
					listEntry.setTitle(titles[i]);
					listEntry.setDrawable(getResources().getDrawable(drawables[i]));
					
					entries.add(listEntry);
					
				}
				listViewAdapter = new ListViewAdapter();
				listview.setAdapter(listViewAdapter);
		
		
		return rootView;
		
	}
private class ListViewAdapter extends BaseAdapter {
		
		private ListViewHolder holder;

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return entries.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			convertView =LayoutInflater.from(getActivity()).inflate(R.layout.item_listview_discover_on_tab, null);
			holder = new ListViewHolder();
			holder.image = (ImageView)convertView.findViewById(R.id.dis_ontab_image);
			holder.title = (TextView)convertView.findViewById(R.id.tv_dis_ontab);
			
			//set title
			if(entries.get(position).getTitle() != null) {
				holder.title.setText(entries.get(position).getTitle());
			}
			
			//set image
			if(entries.get(position).getDrawable() != null) {
				holder.image.setImageDrawable(entries.get(position).getDrawable());
			}
			return convertView;
		}
		
		private class ListViewHolder {
			
			public ImageView image;
			public TextView title;
		}
		
	}

	

}
