package com.patsorn.youthtoday;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

public class SortFilterPage extends Activity {
	ImageButton closeButton;
	//Button closeButton,applyButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sort_filter_page);
		ActionBar actionBar = getActionBar();
		//actionBar.hide();
		
		
		actionBar.setCustomView(R.layout.header_sort_filter); //load your layout
		ImageButton closeButton = (ImageButton) actionBar.getCustomView().findViewById(R.id.close_btn);
		closeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
            	Intent itn = new Intent (getApplicationContext(),DiscoverActivity.class);
        		startActivity(itn);
        		overridePendingTransition(R.anim.fade_in_anim, R.anim.slide_out_down);
        		finish();
            }
    });
 
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM); 
		
		
		
	}
	
	public void toClose(View view){
		Intent itn = new Intent (getApplicationContext(),DiscoverActivity.class);
		startActivity(itn);
		overridePendingTransition(R.anim.fade_in_anim, R.anim.slide_out_down);
		finish();
	}
	
	//public void goTo(View view){
	//	Intent itn = new Intent (this,RegisterPage.class);
	//	startActivity(itn);
	//	finish();
	//}

}
