package com.patsorn.youthtoday;

import java.util.ArrayList;
import java.util.TreeSet;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.nsd.NsdManager.DiscoveryListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;

public class DiscoverActivity extends Activity implements OnScrollListener{
	
	private Context context = null;
	private ListView list = null;
	public View header,footer;
	private ImageButton btnFilter;
	private ImageButton btnSearch;
	private ArrayList<String> data;
	
	
	private ListView listView;
	private int mScrollState = SCROLL_STATE_IDLE;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_discover);
		ActionBar actionBar =  getActionBar();
		//actionBar.hide();
		actionBar.setCustomView(R.layout.header_discover); //load your layout
		ImageButton btnFilter = (ImageButton) actionBar.getCustomView().findViewById(R.id.textview_cancel);
		btnFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
              Intent itn = new Intent (getApplicationContext(),SortFilterPage.class);
  			startActivity(itn);
  			overridePendingTransition(R.anim.slide_in_up, R.anim.fade_out_anim);
  			finish();
            }
    });
 
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM); 
		
		ArrayList<ObItemInstaList> list = new ArrayList<ObItemInstaList>();
		list.add(new ObItemInstaList("	YOUR UPCOMING EVENT", " Title of the event "));
		list.add(new ObItemInstaList("	RECOMMENDED", " Title of the event  "));
		list.add(new ObItemInstaList("	THIS WEEK", " Title of the event "));
		list.add(new ObItemInstaList("	NEXT WEEK", " Title of the event"));
		
		AdapterInstaList adapter = new AdapterInstaList(this, list);
		listView = (ListView) findViewById(R.id.listView1);
		listView.setAdapter(adapter);
		
		//event
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view,
					int position, long id) {
				//data.get(position);
				Intent itn = new Intent (getApplicationContext(),DiscoveryOnTab.class);
				startActivity(itn);
				overridePendingTransition(R.anim.right_to_center, R.anim.fade_out_anim);
				
			}
        	
		});

		listView.setOnScrollListener(this);
	}

	@Override
	public void onScroll(AbsListView list, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// se a lista n�o estiver se movimentando.
		if (mScrollState == OnScrollListener.SCROLL_STATE_IDLE) {
			return;
		}
		// percorre todos os itens vis�veis.
		for (int i = 0; i < visibleItemCount; i++) {
			View listItem = list.getChildAt(i);
			if (listItem == null) {
				break;
			}

			TextView title = (TextView) listItem.findViewById(R.id.title);

			int topMargin = 0;

			// Primeiro item da lista.
			if (i == 0) {
				int top = listItem.getTop();
				int height = listItem.getHeight();

				// Se for negativo a lista foi rolada para cima.
				if (top < 0) {
					
					if (title.getHeight() < (top + height)) {
						topMargin = -top;
					} else {
						
						//topMargin = height - title.getHeight();
						topMargin = title.getHeight() < (top + height) ? -top : (height - title.getHeight());
					}
				}
			}

			// Define a marginTop do titulo.
			((ViewGroup.MarginLayoutParams) title.getLayoutParams()).topMargin = topMargin;
			listItem.requestLayout();
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		mScrollState = scrollState;
	}
	
	public void goDiscover(View view){
		Intent itn = new Intent (getApplicationContext(),DiscoverActivity.class);
		startActivity(itn);
		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
		finish();
	}
 
	 public void goActivity(View view){
		Intent itn = new Intent (getApplicationContext(),PageActivity.class);
		startActivity(itn);
		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
		finish();
	}
	 
	 public void goProfile(View view){
			Intent itn = new Intent (getApplicationContext(),ProfilePage.class);
			startActivity(itn);
			overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
			finish();
		}
	 
	 public void goSortFilter(View view){
			Intent itn = new Intent (getApplicationContext(),SortFilterPage.class);
			startActivity(itn);
			overridePendingTransition(R.anim.slide_in_up, R.anim.fade_out_anim);
			finish();
		}
	 public void goSearch(View view){ 
			Intent itn = new Intent (DiscoverActivity.this,SortFilterPage.class);
			startActivity(itn);
			finish();
		}
	 public boolean onCreateOptionsMenu(Menu menu) {
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.menu_discover, menu);
	        return super.onCreateOptionsMenu(menu);
	    }
	    
	 public boolean onOptionsItemSelected(MenuItem item){
		 int id = item.getItemId();
	        switch (id) {
	            case R.id.menu_discover:
	            	Intent itn1 = new Intent (getApplicationContext(),DiscoverActivity.class);
	        		startActivity(itn1);
	        		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
	        		finish();
	                return true;
	            case R.id.menu_activity:
	            	Intent itn2 = new Intent (getApplicationContext(),PageActivity.class);
	        		startActivity(itn2);
	        		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
	        		finish();
	                return true;
	            case R.id.menu_profile:
	            	Intent it3 = new Intent (getApplicationContext(),ProfilePage.class);
	    			startActivity(it3);
	    			overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
	    			finish();
	                return true;
	            default:
	            	return super.onOptionsItemSelected(item);
	        }
			
	 }

}
