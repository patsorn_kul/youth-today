package com.patsorn.youthtoday;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterInstaList extends BaseAdapter  {
	private ArrayList<ObItemInstaList> itens;
	private LayoutInflater layoutInflater;
	ImageView imageView1;
	RoundImage roundImage;
	
	public AdapterInstaList(Context context, ArrayList<ObItemInstaList> itens) {
		this.itens = itens;
		this.layoutInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return itens.size();
	}

	@Override
	public ObItemInstaList getItem(int arg0) {
		return itens.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		ItemHelper itemHelper;
		if (arg1 == null) {
			itemHelper = new ItemHelper();
			arg1 = layoutInflater.inflate(R.layout.item_discover, null);
			TextView title = (TextView) arg1.findViewById(R.id.title);
			TextView content = (TextView) arg1.findViewById(R.id.content);
			
			itemHelper.title = title;
			itemHelper.content = content;
			
			arg1.setTag(itemHelper);
		} else {
			itemHelper = (ItemHelper) arg1.getTag();
		}
		ObItemInstaList ob = getItem(arg0);

		itemHelper.title.setText(ob.getTitle());
		itemHelper.content.setText(ob.getContent());
		
		//Round Image
		imageView1 = (ImageView)arg1.findViewById(R.id.firstpic);
		Bitmap bm = BitmapFactory.decodeResource(arg1.getResources(), R.drawable.image7);
		roundImage = new RoundImage(bm);
		imageView1.setImageDrawable(roundImage);



		return arg1;
	}

	private class ItemHelper {
		TextView title, content,freetext;
		ImageView firstpic,freepic;
	}

}

