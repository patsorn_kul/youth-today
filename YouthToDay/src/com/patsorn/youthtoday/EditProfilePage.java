package com.patsorn.youthtoday;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class EditProfilePage extends Activity  {
	private TextView textCancel;
	
	private ListView listview;
	private ListViewAdapter listViewAdapter;
	private ImageView frontImage,blurImage;
	RoundImage roundImage;
	FrameLayout frame;
	
	private ArrayList<ListEntry> entries;
	private int[] drawables = {
			
			R.drawable.icon_tab_profile_editprofile,
			R.drawable.icon_tab_profile_locationx
			
		
	};
	private String[] titles = {
			" Gary Lim",
			"Singapore, Singapore"
		
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_profile_page);
		ActionBar actionBar = getActionBar();
		//actionBar.hide();
		actionBar.setCustomView(R.layout.header_edit_profile); //load layout
		TextView textCancel = (TextView) actionBar.getCustomView().findViewById(R.id.textview_cancel);
		textCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
            	Intent itn = new Intent (getApplicationContext(),ProfilePage.class);
        		startActivity(itn);
        		overridePendingTransition(R.anim.fade_in_anim, R.anim.center_to_right);
        		finish();
            }
    });
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM);
		
		//Blur image
		frontImage = (ImageView)findViewById(R.id.front_img);
		Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.pic2);
		roundImage = new RoundImage(bm);
		frontImage.setImageDrawable(roundImage);
		
		blurImage = (ImageView)findViewById(R.id.blurimage);
		blurImage.setAlpha(15);
	
		//view matching
		listview = (ListView) findViewById(R.id.listView1);
		//data
		entries = new ArrayList<ListEntry>();
		
		//add data
		for(int i = 0;i<titles.length;i++){
			ListEntry listEntry = new ListEntry();
			listEntry.setTitle(titles[i]);
			listEntry.setDrawable(getResources().getDrawable(drawables[i]));
			
			entries.add(listEntry);
			
		}
		listViewAdapter = new ListViewAdapter();
		listview.setAdapter(listViewAdapter);
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
			}
			
		});

	}
	private class ListViewAdapter extends BaseAdapter {
		
		private ListViewHolder holder;
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return entries.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			convertView =LayoutInflater.from(EditProfilePage.this).inflate(R.layout.item_listview_edit_profile, null);
			holder = new ListViewHolder();
			holder.image = (ImageView)convertView.findViewById(R.id.edit_profile_image);
			holder.title = (TextView)convertView.findViewById(R.id.tv_edit_profile);
			
			//set title
			if(entries.get(position).getTitle() != null) {
				holder.title.setText(entries.get(position).getTitle());
			}
			
			//set image
			if(entries.get(position).getDrawable() != null) {
				holder.image.setImageDrawable(entries.get(position).getDrawable());
			}

			
			return convertView;
		}
private class ListViewHolder {
			
			public ImageView image;
			public TextView title;
		}
	}

	
	public void goDiscover(View view){
		Intent itn = new Intent (getApplicationContext(),DiscoverActivity.class);
		startActivity(itn);
		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
		finish();
	}
	
	public void goActivity(View view){
		Intent itn = new Intent (getApplicationContext(),PageActivity.class);
		startActivity(itn);
		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
		finish();
	}
	public void goProfile(View view){
		Intent itn = new Intent (getApplicationContext(),ProfilePage.class);
		startActivity(itn);
		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
		finish();
	}
	public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }
    
 public boolean onOptionsItemSelected(MenuItem item){
	 int id = item.getItemId();
        switch (id) {
            case R.id.menu_discover:
            	Intent itn1 = new Intent (getApplicationContext(),DiscoverActivity.class);
        		startActivity(itn1);
        		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
        		finish();
                return true;
            case R.id.menu_activity:
            	Intent itn2 = new Intent (getApplicationContext(),PageActivity.class);
        		startActivity(itn2);
        		overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
        		finish();
                return true;
            case R.id.menu_profile:
            	Intent it3 = new Intent (getApplicationContext(),ProfilePage.class);
    			startActivity(it3);
    			overridePendingTransition(R.anim.fade_in_anim, R.anim.fade_out_anim);
    			finish();
                return true;
            default:
            	return super.onOptionsItemSelected(item);
        }
		
 }

}
