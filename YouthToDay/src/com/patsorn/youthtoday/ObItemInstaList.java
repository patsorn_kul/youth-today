package com.patsorn.youthtoday;

import android.media.Image;

public class ObItemInstaList {
	private String title, content,freetext;
	private Image firstpic,freepic;

	public ObItemInstaList() {
		this("", "");
	}

	public ObItemInstaList(String title, String content) {
		super();
		this.title = title;
		this.content = content;
		
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	public String getText(){
		return freetext;
		
	}
	public void setText(String freetext){
		this.freetext = freetext;
	}
	
	

	@Override
	public String toString() {
		return "ObItemInstaList [title=" + title + ", content=" + content + "]";
	}

}
