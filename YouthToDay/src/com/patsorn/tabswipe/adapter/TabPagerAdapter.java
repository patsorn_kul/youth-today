package com.patsorn.tabswipe.adapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.patsorn.youthtoday.FragmentTicket;
import com.patsorn.youthtoday.FragmentInfoTicket;


public class TabPagerAdapter extends FragmentPagerAdapter {
	
	public TabPagerAdapter(FragmentManager fm) {
		super(fm);
		
	}

	@Override
	public Fragment getItem(int index) {
		
		switch(index){
		case 0:
			//Ticket fragment activity
			return new FragmentTicket();
		case 1:
			//info ticket fragment activity
			return new FragmentInfoTicket();
			
		}
		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 2 ;
	}

}
